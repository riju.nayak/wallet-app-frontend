import React from 'react';
import './App.css';
import Wallet from "./Wallet/Wallet";
import Header from "./Header/Header";
import {Container} from "reactstrap";
import Transactions from "./Wallet/Transactions/Transactions";
import {BrowserRouter as Router, Route} from 'react-router-dom';
import TransactionModel from "./Wallet/Transaction/TransactionsModel";


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            transactions: []
        }
    }

    componentDidMount() {
        this.updateAllTransaction();
    }

    updateAllTransaction = () => {
        TransactionModel.fetchAll(1, 1)
            .then(transactions => {
                this.setState({
                    transactions: transactions
                })
            })
    };


    render() {
        return (
            <div className="App">
                <Container>
                    <Header/>
                    <Router>
                        <Route exact path='/' component={Wallet}/>
                        <Route exact path='/transactions'
                               render={() => {
                                   this.updateAllTransaction();
                                   return <Transactions  transactions={this.state.transactions}/>
                               }}/>
                    </Router>
                </Container>
                <Container>

                </Container>
            </div>
        );

    }
}

export default App;
