import axios from "axios";

class WalletModel {
    constructor({balance, name}) {
        this._balance = balance || 0;
        this._name = name;
    }

    balance() {
        return this._balance;
    }

    name() {
        return this._name;
    }

    static async fetchWallet(walletId) {
        const response = await axios.get(`https://justice-league-wallet.herokuapp.com/wallets/` + walletId);
        return new WalletModel(response.data);
    }

    static async deposit(walletId, walletModel) {
        const response = await axios.post(`https://justice-league-wallet.herokuapp.com/wallets/` + walletId + '/transactions', walletModel).then(response => {
            return new WalletModel(response.data);
        });
        return response;
    }
}

export default WalletModel;
