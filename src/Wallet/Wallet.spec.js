import Wallet from './Wallet';
import React from 'react';
import {shallow, mount} from "enzyme";
import Dashboard from "./Dashboard/Dashboard";
import WalletModel from "./WalletModel";
import WalletCards from "./WalletCards/WalletCards";
import TransactionModel from "./Transaction/TransactionsModel";

const singleTransaction = {
    id: 1,
    amount: 50,
    type: "credit",
    remarks: "snacks"
};
const multipleTransaction = [
    {
        id: 1,
        amount: 50,
        type: "credit",
        remarks: "snacks"
    },
    {
        id: 2,
        amount: 50,
        type: "debit",
        remarks: "drinks"
    }
];
let wallet = {
    id: 1,
    balance: 100,
    name: 'Shivam',
};

describe('Wallet',() => {
    it('should render without crashing', () => {
        shallow(<Wallet/>);
    });

    it('should render dashboard', () => {
        const dashboardElement = shallow(<Dashboard wallet = {new WalletModel({balance: 100, name:"Shivam"})}/>);

        expect(dashboardElement).toHaveLength(1);
    });

    it('should render wallet cards', () => {
        const mockedFunction = jest.fn();
        const walletCardsElement = shallow(<WalletCards onTransaction = {mockedFunction}/>);

        expect(walletCardsElement).toHaveLength(1);
    });

    it('should update balance', () => {
        const walletElement = shallow(<Wallet transactions={multipleTransaction}/>);
        WalletModel.fetchWallet = jest.fn();
        TransactionModel.fetchRecentTransaction = jest.fn();
        WalletModel.fetchWallet.mockResolvedValue(wallet);
        TransactionModel.fetchRecentTransaction.mockResolvedValue(multipleTransaction)

        walletElement.instance().updateBalance();

        expect(WalletModel.fetchWallet).toHaveBeenCalledWith(1);
        expect(TransactionModel.fetchRecentTransaction).toHaveBeenCalledWith(1);

    });

    it('should fetch balance', async () => {
        const walletElement = shallow(<Wallet transactions={multipleTransaction}/>);
        WalletModel.fetchWallet = jest.fn();
        TransactionModel.fetchRecentTransaction = jest.fn();
        WalletModel.fetchWallet.mockResolvedValue(wallet);
        TransactionModel.fetchRecentTransaction.mockResolvedValue(multipleTransaction)

        walletElement.instance().componentDidMount();

        expect(WalletModel.fetchWallet).toHaveBeenCalledWith(1);
        expect(TransactionModel.fetchRecentTransaction).toHaveBeenCalledWith(1);

    });
});


