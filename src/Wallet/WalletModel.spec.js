import WalletModel from "./WalletModel";
import axios from "axios";

jest.mock('axios');
describe('Wallet balance', () => {
    beforeEach(() => {
        axios.get.mockResolvedValue({data: {}});
    });

    it('should create wallet model', () => {
       const walletModel = new WalletModel({balance: 100});

       expect(walletModel.balance()).toEqual(100);
    });

    it('should fetch from api', () => {
        WalletModel.fetchWallet(1);

        expect(axios.get).toHaveBeenCalledWith('https://justice-league-wallet.herokuapp.com/wallets/1')
    });

    it('should wrap the api results and return wallet', async () => {
        axios.get.mockResolvedValue({data:{"id":1,"balance":100,"name":"John"}});
        const wallet = await WalletModel.fetchWallet(1);

        expect(wallet.balance()).toEqual(100);
    });

    it('should deposit amount', async () => {
        axios.post.mockResolvedValue({data: {"id": 1, "balance": 200, "name": "John"}});
        const wallet = await WalletModel.deposit(1, {amount: 100, remarks: 'Snacks', transactiontype: 'CREDIT'});

        expect(wallet.balance()).toEqual(200);
    });
});
