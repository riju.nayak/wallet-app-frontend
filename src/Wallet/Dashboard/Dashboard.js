import React, {Component} from 'react';
import './Dashboard.css';

class Dashboard extends Component {

    render() {
        const currencySymbol = '\u20B9';
        const minBalance = this.props.wallet.balance() < 2000;
        const minBalanceCSS = minBalance?'red-color':'';
        const lowBalanceError = minBalance?<div id='low-balance-error' className='low-balance-error'>Please maintain balance of {currencySymbol} 2000</div>:<div></div>;
        return (
            <div className='dashboard'>
                <div className='profile-photo'></div>
                <div id = 'user-name' className='user-name'>{this.props.wallet.name()}</div>
                <div id='show-balance' className='show-balance'>Balance: <span id='balance' className={minBalanceCSS}>{currencySymbol} {this.props.wallet.balance()}</span>
                {lowBalanceError}</div>
            </div>
        );
    }
}

export default Dashboard;
