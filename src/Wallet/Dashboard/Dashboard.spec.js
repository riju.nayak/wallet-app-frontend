import React from 'react';
import {shallow} from "enzyme";
import Dashboard from "./Dashboard";
import WalletModel from "../WalletModel";

describe('Dashboard', () => {
    it('should display balance', () => {
        const dashboardElement = shallow(<Dashboard wallet = {new WalletModel({balance: 100})}/>);
        const balanceElement = dashboardElement.find('#show-balance');

        expect(balanceElement.find('#balance').props().children[2]).toEqual(100);
    });

    it('should display balance in red if less than 2000', () => {
        const dashboardElement = shallow(<Dashboard wallet = {new WalletModel({balance: 100})}/>);
        const balanceElement = dashboardElement.find('#show-balance');

        expect(balanceElement.find('#balance').props().className).toEqual('red-color')
    });

    it('should display warning if balance is low', () => {
        const dashboardElement = shallow(<Dashboard wallet = {new WalletModel({balance: 100})}/>);
        const balanceElement = dashboardElement.find('#show-balance');

        expect(balanceElement.find('#low-balance-error')).toHaveLength(1);
    });

    it('should not display warning if balance is not less than 2000', () => {
        const dashboardElement = shallow(<Dashboard wallet = {new WalletModel({balance: 2000})}/>);
        const balanceElement = dashboardElement.find('#show-balance');

        expect(balanceElement.find('#low-balance-error')).toHaveLength(0);
    });

    it('should display user name', () => {
        const dashboardElement = shallow(<Dashboard wallet = {new WalletModel({balance: 2000, name: 'Jon Don'})}/>);
        const userNameElement = dashboardElement.find('#user-name');

        expect (userNameElement.text()).toEqual('Jon Don');
    });
});
