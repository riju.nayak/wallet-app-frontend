import React from 'react';
import WalletModel from "./WalletModel";
import PropTypes from "prop-types";
import './wallet.css'
import Dashboard from "./Dashboard/Dashboard";
import WalletCards from "./WalletCards/WalletCards";
import Transaction from "./Transaction/Transaction";
import TransactionModel from "./Transaction/TransactionsModel";
import {Button, Container} from 'reactstrap';
import {Link} from "react-router-dom";


class Wallet extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            wallet: new WalletModel({balance: "Loading..."}),
            transactions:[{}]
        }
    };

    componentDidMount() {
        WalletModel.fetchWallet(1).then((wallet) => {
            this.setState({wallet: wallet});
        });

        TransactionModel.fetchRecentTransaction(1).then((transactions)=>{
            this.setState({transactions: transactions})
        });
    }

    updateBalance = () => {
        WalletModel.fetchWallet(1).then((wallet) => {
            this.setState({wallet: wallet});
        });

        TransactionModel.fetchRecentTransaction(1).then((transactions)=>{
            this.setState({transactions: transactions})
        });
    };

    render() {
        return (
            <div id='wallet'>
                <div className="wallet">
                    <Dashboard wallet={this.state.wallet}/>
                    <br/>
                    <div className='wallet-cards'>
                        <WalletCards onTransaction={this.updateBalance} transactionType="CREDIT" cardName="Deposit"/>
                        <WalletCards onTransaction={this.updateBalance} transactionType="DEBIT" cardName="Withdraw"/>
                    </div>
                    <div className='recent-transaction'>
                        <Container>
                            <h6 className='recent-transaction-head'>Recent Transactions</h6>
                            {
                                this.state.transactions.map(transaction =>
                                    <div>
                                        <Transaction key={transaction.id} transaction = {transaction}/>
                                    </div>
                                )
                            }
                            <Link to='/transactions'><Button color="info" size='sm'>View All</Button></Link>
                        </Container>
                    </div>
                </div>
            </div>
        );
    }
}

Wallet.propTypes = {
    transactions: PropTypes.array
};

export default Wallet;




