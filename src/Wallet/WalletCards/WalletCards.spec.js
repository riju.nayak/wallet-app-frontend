import React from 'react';
import {shallow} from "enzyme";
import WalletCards from "./WalletCards";
import WalletModel from "../WalletModel";

describe('Wallet Cards', () => {
    it('should display deposit card', () => {
        const mockedFunction = jest.fn();
        const walletCardElement = shallow(<WalletCards onTransaction = {mockedFunction}/>);
        const depositElement = walletCardElement.find('#deposit-block');

        expect(depositElement).toHaveLength(1);
    });

    it('should show error if amount less than 10', () => {
        const mockedFunction = jest.fn();
        const walletCardElement = shallow(<WalletCards onTransaction = {mockedFunction}/>);
        const amountInputElement = walletCardElement.find('#transactionAmount');

        amountInputElement.simulate('change', {target:{value:1}});

        const errorElement = walletCardElement.find('#deposit-min');
        expect(errorElement.props().className.includes('show')).toEqual(true)
    });

    it('should not show error if amount more than 9', () => {
        const mockedFunction = jest.fn();
        const walletCardElement = shallow(<WalletCards onTransaction = {mockedFunction}/>);
        const amountInputElement = walletCardElement.find('#transactionAmount');

        amountInputElement.simulate('change', {target:{value:10}});

        const errorElement = walletCardElement.find('#deposit-min');
        expect(errorElement.props().className.includes('show')).toEqual(false)
    });

    it('should display success message if transaction is done', async () => {
        const mockedFunction = jest.fn();
        WalletModel.deposit = jest.fn();
        WalletModel.deposit.mockResolvedValue();
        const walletCardElement = shallow(<WalletCards onTransaction={mockedFunction}/>);
        const buttonElement = walletCardElement.find('#deposit');
        const amountInputElement = walletCardElement.find('#transactionAmount');

        amountInputElement.simulate('change', {target:{value:10}});
        buttonElement.simulate('click');

        await Promise.resolve();

        const successMessageElement = walletCardElement.find('#success-message');
        expect(successMessageElement.props().className.includes('show')).toEqual(true)
    });

    it('should change remarks', () => {
        const mockedFunction = jest.fn();
        const walletCardElement = shallow(<WalletCards onTransaction={mockedFunction}/>);
        const remarksField = walletCardElement.find('#transactionRemarks');

        remarksField.simulate('change', {target:{value: 'Snacks'}});

        expect(walletCardElement.state().transactionRemarks).toEqual('Snacks');
    });

    it('should not deposit if error', () => {
        const mockedFunction = jest.fn();
        const walletCardElement = shallow(<WalletCards onTransaction={mockedFunction}/>);

        const depositButton = walletCardElement.find('#deposit');

        const amountField = walletCardElement.find('#transactionAmount');
        amountField.simulate('change', {target:{value:5}});

        depositButton.simulate('click');

        expect(mockedFunction).toHaveBeenCalledTimes(0);
    });

    describe('when transactions for day exceeds 50k', () => {
        beforeEach(() => {
            WalletModel.deposit = jest.fn();
            WalletModel.deposit.mockRejectedValue({response: {data: {message: 'error'}}});
        });

        it('should display "Daily Limit Exceeded" error', async () => {
            const walletCardElement = shallow(<WalletCards onTransaction={jest.fn()}/>);
            simulateMaxAmountDeposit(walletCardElement);

            await new Promise((resolve) => setTimeout(resolve, 1));

            const errorMessageElement = walletCardElement.find('#error-message');
            expect(errorMessageElement.props().children).toEqual('error');
        });

        it('should hide error message after two seconds', async () => {
            jest.useFakeTimers();
            const walletCardElement = shallow(<WalletCards onTransaction={jest.fn()}/>);
            simulateMaxAmountDeposit(walletCardElement);

            await Promise.resolve();
            jest.runAllTimers();

            const errorMessageElement = walletCardElement.find('#error-message');
            expect(errorMessageElement.props().className).toContain('hide');
        });

        function simulateMaxAmountDeposit(walletCardElement) {
            const amountField = walletCardElement.find('#transactionAmount');
            amountField.simulate('change', {target: {value: 50001}});

            const depositButton = walletCardElement.find('#deposit');
            depositButton.simulate('click');
        }
    });
});
