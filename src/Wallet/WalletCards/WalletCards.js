import React, {Component} from 'react';
import './WalletCards.css';
import WalletModel from "../WalletModel";
import {Container} from "reactstrap";
import PropTypes from "prop-types";

class WalletCards extends Component {
    constructor(props) {
        super(props);
        this.state = {
            transactionAmount: '',
            transactionRemarks: '',
            showError: false,
            showSuccess: false,
            limitExceeded: false,
            errorMessage: "",
            btnDisable: false
        }
    }

    changeTransactionAmount = (event) => {
        const {value: amount} = event.target;
        const showError = amount < 10;
        this.setState({
            showError: showError,
            transactionAmount: amount
        });
    };

    changeTransactionRemarks = (event) => {
        const {value: remarks} = event.target;
        this.setState({
            transactionRemarks: remarks
        })
    };

    performTransaction = () => {
        this.setState({
            btnDisable: true
        });
        if (this.state.showError) {
            return;
        }
        WalletModel.deposit(1, {
            amount: this.state.transactionAmount,
            remarks: this.state.transactionRemarks,
            transactiontype: this.props.transactionType
        }).then(() => {
            this.props.onTransaction();
            this.setState({
                showSuccess: true,
                transactionAmount: '',
                transactionRemarks: ''
            }, this.hideNotification);
        }).catch((error) => {
            this.setState({
                limitExceeded: true,
                errorMessage: error.response.data.message
            }, this.hideNotification);
        });
    };

    hideNotification = () => {
        setTimeout(() => {
            this.setState({
                showSuccess: false,
                limitExceeded: false,
                btnDisable: false

            });
        }, 2000);
    };

    render() {
        const errorClass = this.state.showError ? 'show' : 'hide';
        const inputStatus = this.state.showSuccess || this.state.limitExceeded ? 'hide' : '';
        const successStatus = this.state.showSuccess ? 'show' : 'hide';
        const limitExceedStatus = this.state.limitExceeded ? 'show' : 'hide';
        return (
            <div className='cards' id='deposit-block'>
                <Container>
                    <div className={`deposit-label ${inputStatus}`}>{this.props.cardName}</div>
                    <div className={`input-div ${inputStatus}`}>
                        <span className='label'>Amount: </span>
                        <input type='number'
                               id='transactionAmount'
                               min='10' step='500'
                               onChange={this.changeTransactionAmount}
                               value={this.state.transactionAmount}/>
                    </div>
                    <span id='deposit-min'
                          className={`min-deposit-error ${errorClass}`}>Minimum Amount 10 is allowed</span>
                    <div className={`input-div ${inputStatus}`}>
                        <span className='label'>Remarks: </span>
                        <input type='text'
                               id='transactionRemarks'
                               onChange={this.changeTransactionRemarks}
                               value={this.state.transactionRemarks}
                               maxLength='10'/>
                    </div>
                    <button disabled={this.state.btnDisable} id='deposit' className={`${inputStatus}`} onClick={this.performTransaction}>Submit</button>
                    <div id='success-message' className={`success ${successStatus}`}/>
                    <div className={`success-message ${successStatus}`}>Success</div>
                    <div id='limit-exceed-message' className={`error ${limitExceedStatus}`}/>
                    <div id = 'error-message' className={`limit-exceed-message ${limitExceedStatus}`}>{this.state.errorMessage}</div>
                </Container>
            </div>

        );
    }
}

WalletCards.propTypes = {
    transactionType: PropTypes.string,
    cardName: PropTypes.string,
    onTransaction: PropTypes.func
};

export default WalletCards;
