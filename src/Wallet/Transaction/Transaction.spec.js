import React from 'react';
import {shallow} from "enzyme";
import Transaction from './Transaction';

const creditTransaction = {
    id: 1,
    amount: 50,
    transactiontype: "CREDIT",
    remarks: "Drinks",
    createdon: "2019-07-16T07:15:16.197Z"
};
const debitTransaction = {
    id: 1,
    amount: 50,
    transactiontype: "DEBIT",
    remarks: "Drinks",
    createdon: "2019-07-16T07:15:16.197Z"
};
const creditTransactionWithNoCreatedOn = {
    id: 1,
    amount: 50,
    transactiontype: "CREDIT",
    remarks: "Drinks",
};

describe('Transaction',() => {
    it('should render without crashing', () => {
        shallow(<Transaction key = {creditTransaction.id} transaction = {creditTransaction} />);
    });

    it('should render transaction data', () => {
        const transactionWrapper = shallow(
            <Transaction key = {creditTransaction.id} transaction = {creditTransaction} />);
        const transactionElement = transactionWrapper.find('#transaction');

        expect(transactionElement.find('#transaction-id').props().children[1]).toEqual(1);
        expect(transactionElement.find('#transaction-amount').props().children[1]).toEqual(50);
        expect(transactionElement.find('#transaction-remarks').props().children).toEqual("Drinks");
    });

    it('should render amount in green for credit', () => {
        const transactionWrapper = shallow(
            <Transaction key = {creditTransaction.id} transaction = {creditTransaction} />);
        const transactionElement = transactionWrapper.find('#transaction');

        expect(transactionElement.find('#transaction-amount').props().className).toContain("credit");
    });

    it('should render amount in red for debit', () => {

        const transactionWrapper = shallow(
            <Transaction key = {creditTransaction.id} transaction = {debitTransaction} />);
        const transactionElement = transactionWrapper.find('#transaction');

        expect(transactionElement.find('#transaction-amount').props().className).toContain("debit");
    });

    it('should format date time in the string', () => {
        const transactionWrapper = shallow(
            <Transaction key = {debitTransaction.id} transaction = {debitTransaction} />);
        const transactionElement = transactionWrapper.find('#transaction');

        expect(transactionElement.find('#transaction-date').props().children).toContain("16 July 2019 07:15:16");
    });

    it('should not format date time if no createdon provided', () => {
        const transactionWrapper = shallow(
            <Transaction key = {creditTransactionWithNoCreatedOn.id} transaction = {creditTransactionWithNoCreatedOn} />);
        const transactionElement = transactionWrapper.find('#transaction');

        console.log(transactionElement.find('#transaction-date').props())

        expect(transactionElement.find('#transaction-date').props().children).toContain('');
    });
});
