import axios from "axios";
import TransactionsModel from './TransactionsModel'

jest.mock('axios');
describe('Transaction Model', () => {
    beforeEach(() => {
        axios.get.mockResolvedValue({data: {}});
    });

    it('should get transactions of particular wallet', () => {
        TransactionsModel.fetchRecentTransaction(1);

        expect(axios.get).toHaveBeenCalledWith('https://justice-league-wallet.herokuapp.com/wallets/1/transactions?pageNumber=1&pageSize=7')
    });

    it('should return list transactions', async () => {
        axios.get.mockResolvedValue({data:[]});
        const transactions = await TransactionsModel.fetchRecentTransaction(1);


        expect(transactions).toHaveLength(0);
    });

    it('should return all transactions', async () => {
        axios.get.mockResolvedValue({data: []});
        const transactions = await TransactionsModel.fetchAll(1, 1);

        expect(transactions).toHaveLength(0);
    });
});
