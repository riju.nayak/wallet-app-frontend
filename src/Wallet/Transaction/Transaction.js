import React from 'react';
import PropTypes from "prop-types";
import {Card, Col, Container, Row} from "reactstrap";
import './Transaction.css'
import moment from "moment";

function Transaction(props){

    const typeClass = props.transaction.transactiontype === 'CREDIT' ? 'credit' : 'debit';
    const currencySymbol = '\u20B9';

    function formatDate() {
        if(props.transaction.createdon){
            let date = new Date(props.transaction.createdon);
            return moment(date).format('DD MMMM YYYY hh:mm:ss');
        }
        return '';
    }

    return (
        <Container>
            <div>
                <Card id = 'transaction' className="transaction">
                    <Row>
                        <Col className="transaction-details">
                            <span id = 'transaction-date' className='transaction-date'>{formatDate()}</span>
                            <span id = 'transaction-remarks' className='transaction-remarks'>{props.transaction.remarks}</span>
                        </Col>
                        <Col id = 'transaction-id' className="transaction-id">Reference:{props.transaction.id}</Col>
                        <Col id = 'transaction-amount' className={`transaction-amount ${typeClass}`}>{currencySymbol}{props.transaction.amount}</Col>
                    </Row>
                </Card>
            </div>
        </Container>
    );
}

Transaction.propTypes = {
    id: PropTypes.number,
    amount: PropTypes.number,
    type: PropTypes.string,
    remarks: PropTypes.array,
    createdon: PropTypes.string,
};

Transaction.defaultProps = {
    createdon: '',
}

export default Transaction;
