import axios from "axios";

class TransactionModel {
    static get baseUrl() {
        return `https://justice-league-wallet.herokuapp.com`;
    }

    static async fetchRecentTransaction(walletId) {
        const response = await axios.get(`${TransactionModel.baseUrl}/wallets/` + walletId + '/transactions?pageNumber=1&pageSize=7');
        return response.data;
    }

    static async fetchAll(walletId, pageNumber) {
        const response = await axios.get(`${TransactionModel.baseUrl}/wallets/` + walletId +
            '/transactions?pageNumber=' + pageNumber);
        return response.data;
    }
}


export default TransactionModel;
