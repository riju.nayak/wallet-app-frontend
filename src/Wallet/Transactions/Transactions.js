import React, {Component} from 'react';
import {Button} from "reactstrap";
import {Link} from "react-router-dom";
import './Transactions.css';
import {Container} from "reactstrap";
import Transaction from "../Transaction/Transaction";

class Transactions extends Component {
    render() {
        return (
            <div className='Transactions'>
                <Container>
                    <span>Transactions</span>
                    <Link to='/'><Button className='back-btn' color="info" size='sm'>Back</Button></Link>
                    <br/>
                    <br/>
                    {
                        this.props.transactions.map((transaction) =>
                                <div>
                                    <Transaction key = {transaction.id} transaction = {transaction}/>
                                </div>)
                    }
                </Container>
            </div>
        );
    }
}

export default Transactions;
