import React from 'react';
import {shallow} from "enzyme";
import Transactions from "./Transactions";
import Transaction from "../Transaction/Transaction";

const transactions = [{
    id: 1,
    amount: 60,
    transactiontype: "DEBIT",
    remarks: "Drinks",
    createdon: new Date().toISOString()
},
    {
        id: 2,
        amount: 100,
        transactiontype: "CREDIT",
        remarks: "Drinks",
        createdon: new Date().toISOString()
    }];
describe('Transactions', () => {
    it('should render transactions', () => {
        const transactionsWrapper = shallow(<Transactions transactions={transactions}/>);

        expect(transactionsWrapper).toHaveLength(1);
    });

    it('should render transaction', () => {

        const transactionsWrapper = shallow(<Transactions transactions={transactions}/>);
        const transactionElement = transactionsWrapper.find(Transaction);

        expect(transactionElement).toHaveLength(2);
    });
});
