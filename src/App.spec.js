import React from 'react';
import {shallow, mount} from 'enzyme';
import App from "./App";
import {MemoryRouter, Route} from "react-router-dom";
import Transactions from "./Wallet/Transactions/Transactions";
import TransactionModel from "./Wallet/Transaction/TransactionsModel";

describe('App', () => {
    it('should render app', () => {
        const appWrapper = shallow(<App/>);
    });

    it('should redirect to transaction page', () => {
        const appWrapper = shallow(<App/>);
        expect(appWrapper.find(Route).at(1).props().render().type).toBe(Transactions);

    });

    it('should update all transactions', () => {
        const appWrapper = shallow(<App/>);
        TransactionModel.fetchAll = jest.fn();

        TransactionModel.fetchAll.mockResolvedValue([])
        appWrapper.instance().updateAllTransaction();

        expect(TransactionModel.fetchAll).toHaveBeenCalledWith(1, 1)
    });
});
